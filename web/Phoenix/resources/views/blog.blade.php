<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <!-- Internet Explorer -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Contenido adaptable -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mascotas - La comunidad más grande de adopción</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="images/fav-icon/icon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

    <!-- Vendor Styles -->

    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="vendor/Camera-master/css/camera.css">
    <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="vendor/WOW-master/css/libs/animate.css">
    <link rel="stylesheet" href="vendor/fancybox/dist/jquery.fancybox.min.css">


    <!-- Style -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/icon/font/font/flaticon.css">
    <link rel="stylesheet" href="fonts/icon/font/2/flaticon.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
  </head>

  <body>
    <div class="main-page-wrapper home-page-two">

      <div class="html-top-content">
        
        <!-- Menú -->
        <header class="theme-main-header">
          <div class="container">
            <div class="menu-wrapper clearfix">
              <div class="logo float-left"><a href="index.html"><img src="images/logo.png" alt="Logo"></a></div>

              <ul class="button-group float-right">
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">home</i> Inicio
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">fingerprint</i> Ingresar
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">person_add</i> Registrarse
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">contact_mail</i> Contacto
                  </a>
                </li>                       
              </ul>           
            </div> 
          </div> 
        </header> 
        <!-- /Menú  -->

				<!-- Banner -->
				<div class="inner-page-banner">
					<div class="opacity">
						<h1>Mascotas  =  familia</h1>
					</div> 
				</div> 
				<!-- /Banner -->

				<div style="padding-top: 100px;">
					
				</div>

				<!-- Blog -->
				<div class="our-blog">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-xs-6">

							<div class="single-blog">
								<div class="image"><img src="images/blog/comida.jpg" alt=""></div>
								<div class="text">
									<h6>Carlos French.</h6>
									<h5><a href="blog-details.html" class="tran3s">Cómo hacer un delicioso almuerzo para tu gato</a></h5>
									<p>Their testimonial videos aren't production quality, but they get message across, cover useful & relevant information with tips.</p>
								</div>
							</div> <!-- /.single-blog -->

						</div>
						<div class="col-lg-3 col-md-4 col-xs-6">

							<div class="single-blog">
								<div class="image"><img src="images/blog/parque.jpg" alt=""></div>
								<div class="text">
									<h6>María Zapata.</h6>
									<h5><a href="blog-details.html" class="tran3s">Cuidados para tu perro en el parque</a></h5>
									<p>Their testimonial videos aren't production quality, but they get message across, cover useful & relevant information with tips.</p>
								</div>
							</div> <!-- /.single-blog -->

						</div>
						<div class="col-lg-3 col-md-4 col-xs-6">
							<div class="single-blog">

								<div class="image"><img src="images/blog/limpieza.jpg" alt=""></div>
								<div class="text">
									<h6>Daniel Castaño.</h6>
									<h5><a href="blog-details.html" class="tran3s">Cómo limpiar tu gato adecuadamente</a></h5>
									<p>Their testimonial videos aren't production quality, but they get message across, cover useful & relevant information with tips.</p>
								</div>
							</div> <!-- /.single-blog -->

						</div>
						<div class="col-lg-3 col-md-4 col-xs-6">

							<div class="single-blog">
								<div class="image"><img src="images/blog/cama.jpg" alt=""></div>
								<div class="text">
									<h6>Sandra Hurtado</h6>
									<h5><a href="blog-details.html" class="tran3s">Cómo hacer una cama para tu mascota</a></h5>
									<p>Their testimonial videos aren't production quality, but they get message across, cover useful & relevant information with tips.</p>
								</div>
							</div> <!-- /.single-blog -->

						</div>
					</div>
					<a href="#" class="load-more tran3s theme-button"><span></span> Ver blog</a>

					<div class="pagination">
					  <a href="#">&laquo;</a>
					  <a class="active" href="#">1</a>
					  <a href="#">2</a>
					  <a href="#">3</a>
					  <a href="#">4</a>
					  <a href="#">5</a>
					  <a href="#">6</a>
					  <a href="#">&raquo;</a>
					</div>
				</div>
				<!-- /Blog -->

				<div style="padding-bottom: 10px;"></div>
			</div> <!-- /.html-top-content -->

      <!-- Footer -->
      <footer>
        <div class="container">
          <div class="footer-data-wrapper">

            <div class="bottom-footer">
              <div class="row">
                <div class="col-lg-6 col-md-7 col-xs-12 text-right pull-right">
                  <ul class="footer-menu">
                    <li><a href="blog.html" class="tran3s">Blog</a></li>
                    <li><a href="#" class="tran3s">Nosotros</a></li>
                    <li><a href="#" class="tran3s">Adopción </a></li>
                    <li><a href="#" class="tran3s">Legal</a></li>
                  </ul>
                  
                  <ul>
                    <li class="search-form" style="margin-right: 40px;">
                    <form action="">
                        <input class="buscar" align="middle" type="text" name="" placeholder="¿Qué deseas buscar?">
                        <button type="submit" >
                          <i class="material-icons">search</i>
                        </button>
                      </form>
                    </li> 
                  </ul><br>

                  <ul class="social-icon">
                    <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                  </ul>
                </div>
                <div class="col-lg-6 col-md-5 col-xs-12 footer-logo">
                  <div class="logo"><a href="index.html"><img src="images/logo.png" alt="Logo"></a></div>
                  <p>Hecho con el &#10084; <strong> hacia los animales</strong></p>
                </div>
              </div> 
            </div> 
          </div>
        </div> 
      </footer>
      <!-- /Footer -->


      <!-- Top Button -->
      <button class="scroll-top tran3s color-one-bg" style="background: black; color: white;">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
      </button>
      <!-- /Top Button -->


    <script type="text/javascript" src="vendor/jquery.2.2.3.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/bootstrap.min.js"></script>
    <script type='text/javascript' src='vendor/Camera-master/scripts/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='vendor/Camera-master/scripts/jquery.easing.1.3.js'></script> 
    <script type='text/javascript' src='vendor/Camera-master/scripts/camera.min.js'></script>
    <script type="text/javascript" src="vendor/WOW-master/dist/wow.min.js"></script>
    <script type="text/javascript" src="vendor/tilt.js/src/tilt.jquery.js"></script>
    <script type="text/javascript" src="vendor/fancybox/dist/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="vendor/contact-form/jquery.form.js"></script>
    <script type="text/javascript" src="vendor/owl-carousel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/theme.js"></script>

    </div> 
  </body>
</html>
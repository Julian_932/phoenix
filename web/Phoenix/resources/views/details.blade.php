<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <!-- Internet Explorer -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Contenido adaptable -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mascotas - La comunidad más grande de adopción</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="images/fav-icon/icon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

    <!-- Vendor Styles -->

    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="vendor/Camera-master/css/camera.css">
    <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="vendor/WOW-master/css/libs/animate.css">
    <link rel="stylesheet" href="vendor/fancybox/dist/jquery.fancybox.min.css">


    <!-- Style -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/icon/font/font/flaticon.css">
    <link rel="stylesheet" href="fonts/icon/font/2/flaticon.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
  </head>

  <body>
    <div class="main-page-wrapper home-page-two">

      <div class="html-top-content">
        
        <!-- Menú -->
        <header class="theme-main-header">
          <div class="container">
            <div class="menu-wrapper clearfix">
              <div class="logo float-left"><a href="index.html"><img src="images/logo.png" alt="Logo"></a></div>

              <ul class="button-group float-right">
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">home</i> Inicio
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">fingerprint</i> Ingresar
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">person_add</i> Registrarse
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="material-icons">contact_mail</i> Contacto
                  </a>
                </li>                       
              </ul>           
            </div> 
          </div> 
        </header> 
        <!-- /Menú  -->

        <!-- Banner -->
        <div class="inner-page-banner">
          <div class="opacity">
            <h1>Cuidados para tu perro en el parque</h1>
          </div> 
        </div> 
        <!-- /Banner -->

        <!-- Details -->
        <div class="blog-details blog-v3">
          <div class="container">
            <div class="wrapper">
              <div class="blog-main-post">
                <img src="images/blog/9.jpg" alt="">
                <h6>María Zapata</h6>
                <h3>Ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                <p>Dolor sit amet, consectetur adipisicing elit. Rem, sequi. Earum, iure ab deserunt quam debitis maxime, error assumenda officiis cum sit facilis omnis totam.</p> <br>

                <div class="center" style="justify-content: center;">

                  <img src="images/blog/parque.jpg" width="70%">                
                </div>

                <br>

                <p>Consectetur adipisicing elit. Nemo recusandae laudantium voluptatem eligendi quis quo, quibusdam nostrum libero quidem reiciendis sequi non reprehenderit ipsa dolorem perspiciatis quod tempora tempore officia laboriosam? Consectetur cum quia quis omnis aperiam, tempore iure quisquam sequi doloremque, blanditiis. Quasi, consectetur?</p><br>
                
                <p><h5>Sit amet, consectetur adipisicing elit. Autem vitae necessitatibus voluptate quidem excepturi fugiat ipsam expedita perspiciatis quasi quos. Blanditiis ex, vero nesciunt. Nostrum laboriosam doloribus voluptates porro at, officiis blanditiis, molestiae aspernatur odio magnam quisquam inventore sit voluptatum corporis illum deleniti ipsam nemo iusto veniam quidem repellendus. Incidunt. </h5></p> 

                <p>Adipisicing elit. Facilis illo quod, nostrum reprehenderit beatae obcaecati animi hic inventore architecto veniam? Velit veritatis sequi veniam dignissimos unde consequatur nihil itaque.</p>
                
                <h2>Vero minima non, eum optio magni laudantium doloremque debitis, in quae, nobis ut.</h2>

                <p>Adipisicing elit. Facilis illo quod, nostrum reprehenderit beatae obcaecati animi hic inventore architecto veniam? Velit veritatis sequi veniam dignissimos unde consequatur nihil itaque.</p>
                
              </div> <!-- /.blog-main-post -->
              <div class="tag-option clearfix">
                <ul class="float-left">
                  <li>Categoría:</li>
                  <li><a href="" class="tran3s">Cuidado,</a></li>
                  <li><a href="" class="tran3s">Alimentación,</a></li>
                  <li><a href="" class="tran3s">Diversión,</a></li>
                  <li><a href="" class="tran3s">Crecimiento</a></li>
                </ul>
                <ul class="float-right">
                  <li>Compartir:</li>
                  <li><a href="" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  <li><a href="" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                </ul>
              </div> <!-- /.tag-option -->
            </div> <!-- /.wrapper -->
          </div> <!-- /.container -->

          <<div class="comentarios">
            <div class="container">
              <div class="wrapper">
                <div id="disqus_thread"></div>
                  <script>
                  (function() { // DON'T EDIT BELOW THIS LINE
                  var d = document, s = d.createElement('script');
                  s.src = 'https://perritos-1.disqus.com/embed.js';
                  s.setAttribute('data-timestamp', +new Date());
                  (d.head || d.body).appendChild(s);
                  })();
                  </script>
                  <noscript>Por favor actica el JavaScript para ver los <a href="https://disqus.com/?ref_noscript">comentarios.</a></noscript>
              </div> <!-- /.wrapper -->
            </div> <!-- /.container -->
          </div> <!-- /.comentarios -->
        </div> <!-- /.detalles-blog -->
        <!-- /Details -->
      </div> <!-- /.html-top-content -->


      <!-- Footer -->
      <footer>
        <div class="container">
          <div class="footer-data-wrapper">

            <div class="bottom-footer">
              <div class="row">
                <div class="col-lg-6 col-md-7 col-xs-12 text-right pull-right">
                  <ul class="footer-menu">
                    <li><a href="blog.html" class="tran3s">Blog</a></li>
                    <li><a href="#" class="tran3s">Nosotros</a></li>
                    <li><a href="#" class="tran3s">Adopción </a></li>
                    <li><a href="#" class="tran3s">Legal</a></li>
                  </ul>
                  
                  <ul>
                    <li class="search-form" style="margin-right: 40px;">
                    <form action="">
                        <input class="buscar" align="middle" type="text" name="" placeholder="¿Qué deseas buscar?">
                        <button type="submit" >
                          <i class="material-icons">search</i>
                        </button>
                      </form>
                    </li> 
                  </ul><br>

                  <ul class="social-icon">
                    <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                  </ul>
                </div>
                <div class="col-lg-6 col-md-5 col-xs-12 footer-logo">
                  <div class="logo"><a href="index.html"><img src="images/logo.png" alt="Logo"></a></div>
                  <p>Hecho con el &#10084; <strong> hacia los animales</strong></p>
                </div>
              </div> 
            </div> 
          </div>
        </div> 
      </footer>
      <!-- /Footer -->


      <!-- Top Button -->
      <button class="scroll-top tran3s color-one-bg" style="background: black; color: white;">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
      </button>
      <!-- /Top Button -->


    <script type="text/javascript" src="vendor/jquery.2.2.3.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/bootstrap.min.js"></script>
    <script type='text/javascript' src='vendor/Camera-master/scripts/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='vendor/Camera-master/scripts/jquery.easing.1.3.js'></script> 
    <script type='text/javascript' src='vendor/Camera-master/scripts/camera.min.js'></script>
    <script type="text/javascript" src="vendor/WOW-master/dist/wow.min.js"></script>
    <script type="text/javascript" src="vendor/tilt.js/src/tilt.jquery.js"></script>
    <script type="text/javascript" src="vendor/fancybox/dist/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="vendor/contact-form/jquery.form.js"></script>
    <script type="text/javascript" src="vendor/owl-carousel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/theme.js"></script>

    </div> 
  </body>
</html>